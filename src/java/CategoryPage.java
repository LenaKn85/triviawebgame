import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */


@WebServlet(name = "CategoryPage", urlPatterns = {"/CategoryPage"})
public class CategoryPage extends HttpServlet {

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
   HttpSession session = request.getSession();
   
    String formURL =
          "QuestionPage";
        // Pass URLs that reference own site through encodeURL.
        formURL = response.encodeURL(formURL);
    
        String filename = "/WEB-INF/Categories.txt"; //file location
        Map mMap = new HashMap();
        int count = 0;
        String username;

        ServletContext context = getServletContext();
        PrintWriter out = response.getWriter();
        
        username = (String)session.getAttribute("username");
        
        InputStream is = context.getResourceAsStream(filename);
        if (is != null) {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader reader = new BufferedReader(isr);
            String text = "";

            while ((text = reader.readLine()) != null) { //reads the file line by line
                mMap.put(count, text);
                count++;
            }
        }

        Iterator iter = mMap.entrySet().iterator();
   
        response.setContentType("text/html;charset=UTF-8");
        try {
            String docType
                    = "<!DOCTYPE html"
                    + "transitional//en\">\n";

            out.println(docType
                    + "<html>\n"
                    + "<head><title>TriviaGame for Masters</title></head>\n"
                    + "<link rel=\"stylesheet\" href=\"categories.css\" type=\"text/css\">\n"
                    + "<div style=\"border: 0; width: 130px; height: 20px;position:absolute;\n"
                    + "top: 0; right:0; text-align: right; margin-right: 4px;\">\n"
                    + username
                    + "</div>\n"
                    + "<h1>Categories</h1>\n"
                    + "<form action=\"QuestionPage\" >\n");

            while (iter.hasNext()) {
                Map.Entry mEntry = (Map.Entry) iter.next();
                String catName = (String) mEntry.getValue();

                out.println("<div class=\"img_block\">\n"
                        + "<div class=\"checkbox\"><input type=\"checkbox\" name=\"" + catName + "\"></div>\n"
                        + "<div class=\"titles\"><h3>" + catName + "</h3></div>\n"
                        + "<div class=\"img_block_inner\">\n"
                        + "<div class=\"image\">\n");
                if (catName.equals("Colleges & Uni.")) {
                    out.println("<img src = \"images/Colleges & Universities.jpg\"/>");
                } else {
                    out.println("<img src = \"images/" + catName + ".jpg\"/>");
                }
                out.println("</div>\n"
                        + "<div class=\"radiobn\">\n"
                        + "<input type=\"radio\" checked name=\"" + catName + "_diff\" value=\"Easy\">Easy<br>\n"
                        + "<input type=\"radio\" name=\""+ catName + "_diff\" value=\"Medium\">Medium<br>\n"
                        + "<input type=\"radio\" name=\""+ catName + "_diff\" value=\"Hard\">Hard\n"
                        + "</div>\n"
                        + "</div>\n"
                        + "</div>\n");
            }
            out.println("<p><div>\n"
                    + "<input type=\"submit\" value=\"PLAY RIGHT NOW\">\n"
                    + "</form>\n"
                    + "</div><p>\n"
                    + "</body></html>");
        } finally {
            out.close();
        }
    }
        
        // it is my variant of code
//        String docType =
//      "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " +
//      "Transitional//EN\">\n";
//    out.println(docType +
//                "<HTML>\n" +
//                "<HEAD><TITLE>" + "Category page"+ "</TITLE>\n"+
//                "<link rel=\"stylesheet\" type=\"text/css\" href=\"category.css\">\n"+
//                "</HEAD>\n" +
//                "<BODY>\n" +
//                "<H1>" + "Choose Categories:"+ "</H1>");
//        
//        //when user will choose Categoryes by clicking on checkboxes and cliks SUBMINT button "PLAY" they will sends with this form to other Servlet
//        out.print("<FORM Action=\""+formURL+"\">");
//        
//       out.print("<table style=\"width:100%\">");
//      
//       for(int i=0;i<categoryArrstr.length;i++){
//           out.print("<tr>");
//           out.print("<td>");
//            out.print("<INPUT TYPE=\"checkbox\" id=\""+"cb1\" NAME=\"category_name\" VALUE=\"" +categoryArrstr[i]+"\">"+"<label for=\"cb7\">"+categoryArrstr[i]+"</label>");
//             out.print("</td>");
//            out.print("<tr>");
//       }
//       
//       out.print("</table>");
//      
//       out.print("<p>\n");
//       out.print("<CENTER>");
//       out.print("<INPUT TYPE=\"SUBMIT\" VALUE=\"PLAY\" >");
//       out.print("</p>\n");
//       out.print("</CENTER>");
//       out.print("</FORM>");
//       
//
//          
//         
//          out.println("</BODY></HTML>");
    

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

  
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
