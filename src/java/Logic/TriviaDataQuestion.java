package Logic;

import java.io.Serializable;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;//for shuffle
import java.util.Scanner;

import java.util.*;
import javax.swing.JFrame;
import java.util.Scanner;

import java.util.Random;
import java.io.File;
import java.io.FileWriter;
import java.io.FileOutputStream;
import java.nio.file.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;

public class TriviaDataQuestion implements Serializable {

    public String question; // Actual question
    public String answer; // Answer to question
    public int value; // Point value of question
    public Question type; //Question type, TrueFalse or Multi
    public String category;
    public String optionalAnswer;

    public TriviaDataQuestion(String category, int open_TF_multi_QS, int value, String question, String optionalAnswers, String rightAnswer) {

        this.question = question;
        this.value = value;
        this.category = category;
        this.answer = rightAnswer;
        this.optionalAnswer = optionalAnswers;
        setTypeOfQuestion(open_TF_multi_QS);//type for know what question are print
    }

    public void setTypeOfQuestion(int type_int) {

        switch (type_int) {

            case 1:

                this.type = new OpenQuestion();
                break;

            case 2:

                this.type = new TrueFalseQuestion();
                break;

            case 3:
                this.type = new MultiChoiceQuestion();
                break;

            default:
                throw new IllegalArgumentException("Incorrect Type");
        }

    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    public int getValue() {
        return value;
    }

    public String getoptionalAnswer() {
        return optionalAnswer;
    }

    public Question getType() {
        return type;
    }

    public void openQuestion() {

        type.printQuestion(getQuestion());

    }

    public int getScore(String answer) {

        int score = 0;

        if (getType() instanceof TrueFalseQuestion) {
            if (answer.charAt(0) == getAnswer().charAt(0)) {
                System.out.println("That is correct!  You get " + getValue() + " points.");
                score = getValue();
            } else {
                System.out.println("Wrong, the correct answer is "
                        + getAnswer());
            }
        }
        if (getType() instanceof OpenQuestion) {
            if (answer.toLowerCase().equals(getAnswer().toLowerCase())) {
                System.out.println("That is correct!  You get " + getValue() + " points.");
                score = getValue();
            } else {
                System.out.println("Wrong, the correct answer is " + getAnswer());
            }
        }
        if (getType() instanceof MultiChoiceQuestion) {
            //compare two different letters positions in answers
            String answerUser = answer.toLowerCase();
            String answerOriginal = this.getAnswer().toLowerCase();
            answerUser = answerUser.replaceAll("\\W", "");//cut all , . / still only letters
            // answerUser =answerUser.replaceAll(",","");
            //answerUser = answerUser.replaceAll(" ","");
            answerOriginal = answerOriginal.replaceAll("\\W", "");
            // answerOriginal= answerOriginal.replaceAll(",","");
            // answerOriginal=answerOriginal.replaceAll(" ","");
            ArrayList<String> first = new ArrayList();
            ArrayList<String> second = new ArrayList();
            first.add(answerUser);
            second.add(answerOriginal);
            second.removeAll(first);
            if (second.size() == 0) {
                System.out.println("That is correct!  You get " + getValue() + " points.");
                score = getValue();
            } //            if (answer.toLowerCase().equals(getAnswer().toLowerCase())) {
            //                System.out.println("That is correct!  You get "+ getValue() + " points.");
            //                score=getValue();
            //            } 
            else {
                System.out.println("Wrong, the correct answer is " + getAnswer());
            }
        }

        return score;

    }// getScore and check if answer is CORRECT

}
