
package Logic;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */


@WebServlet(name = "RemoveFromFile", urlPatterns = {"/RemoveFromFile"})
public class RemoveFromFile extends HttpServlet {

 ArrayList<TriviaDataQuestion> arrListAllQuestions =new ArrayList();//global ArrayList
  String global_path;
  String write_data_path;
  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    
        }
    @Override
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        // Set response content type
        response.setContentType("text/html");
        
        String[] question_ids=request.getParameterValues("questions");
         readAllDataFromFileToArrayList();
         
         //now i can remove from list only questions that admin choosed
         deleteQuestionsFromArrayList(question_ids);
         
         //add others question to file
         addDataToFile(arrListAllQuestions);
      
            String nextPage = "/RemoveQuestionPage";
        RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
        dispatcher.forward(request, response);
    }
    public void addDataToFile(ArrayList list){
  try{
        Path path = Paths.get(write_data_path);

        if (Files.notExists(path)){
              // System.out.println("File not exist");
                 
            File f =new File(write_data_path);
                f.createNewFile();
         }

          path=Paths.get(write_data_path);
   
        //if (Files.exists(path)){
       //System.out.println("File now exist");
      
         //}
      }//end try create new File
    catch(IOException e){
        //e.printStackTrace();
        global_path="IOException in method addDataToFile";
    }
    //now we want Write to File one question
  
  try
      {
         FileOutputStream fileOut =
         new FileOutputStream(write_data_path);
         ObjectOutputStream out = new ObjectOutputStream(fileOut);
         
         out.writeObject(list);
         
         out.close();
         fileOut.close();
        // System.out.println("Serialized data is saved in questions.dat");
      }catch(IOException i)
      {
         // i.printStackTrace();
          global_path="IOException in method addDataToFile FileOutputStream after close";
      }
  
}//end function addDataToFile
   public void deleteQuestionsFromArrayList(String[] arr_index_str){
    
    for(int y=0;y<arr_index_str.length;y++){
             
        
            arrListAllQuestions.remove(Integer.parseInt(arr_index_str[y]));
            
        }
          
     }
    public void readAllDataFromFileToArrayList(){
   
//    String sRootPath = new File("").getAbsolutePath();
//    global_path=sRootPath+"/questions.dat";
  
String path_str = getServletContext().getRealPath("/");
//File file = new File(path+"questions.dat");
global_path=path_str+"\\"+"questions.dat";
path_str=path_str+"\\"+"questions.dat";
  write_data_path=path_str;//I save this String path for future use in method addDataToFile()
ArrayList <TriviaDataQuestion> arrlist = new ArrayList( );
        int count=0;
       Path path = Paths.get(path_str);
        if (Files.notExists(path)){
          global_path="File not exist,can't read)";
        return;
        }      
               //if file EXISTS
         ArrayList<TriviaDataQuestion> arr=new ArrayList();
         boolean check=true;
         FileInputStream fileIn=null;
         ObjectInputStream in=null;
         try{
         fileIn = new FileInputStream(path_str);
         
     
         }
        catch(IOException ios){
           //  ios.printStackTrace();
             global_path="IOException_FileInputStream";
         }
         
         
         try{
         in = new ObjectInputStream(fileIn);
         
     
         }
         catch(IOException io){
            // io.printStackTrace();
              global_path="IOException_ObjectInputStream";
         }
         
        
        try
            {
         
         //count++;
         arr = (ArrayList<TriviaDataQuestion>)in.readObject();
         //arrlist.add();////////////////////////////////////
            arrListAllQuestions=arr;//global ArrayList!!!!!!!!!!!!!!!!!!
            }
         
        catch(EOFException eof){
             check=false;
              //global_path="EOFException_arr"; not real exception works good!!!!!
         }
       catch(IOException i)
       {
        // i.printStackTrace();
         return;
       }
        catch(ClassNotFoundException c){
      
//         System.out.println("Question class not found");
         global_path="Question class not found";
         //c.printStackTrace();
         return;
          }
         
         
         try{
         in.close();
         }
         catch(IOException e){
            // e.printStackTrace();
               global_path="IOException_close_in";
         }
        try{
         fileIn.close();
         }
         catch(IOException ee){
           //  ee.printStackTrace();
             global_path="IOException_filein_close";
         }
    

}


}
