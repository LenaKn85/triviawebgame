package Logic;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
@WebServlet(name = "SuccessAddedQuestion", urlPatterns = {"/SuccessAddedQuestion"})
public class SuccessAddedQuestion extends HttpServlet {

    String[] categoryArrstr = {"Art & Artists", "Architecture", "Ancient History", "The Bible", "Cities", "Cinema", "Animal Kingdom", "Astronomy and Space", "Colleges and Universities", "Modern History"};

    Map<Integer, String> testMap = new HashMap<Integer, String>();

    ArrayList<TriviaDataQuestion> arrListAllQuestions = new ArrayList();//global ArrayList

    TriviaDataQuestion TDQ = null;

    String global_path;
    String write_data_path;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        String username;
        username = (String) session.getAttribute("username");

        String kindQuestion = request.getParameter("kind");
        String category = request.getParameter("category_name");
        String value = request.getParameter("values");
        int value_int = Integer.parseInt(value);
        String question = request.getParameter("question");
        String optional_answer = request.getParameter("optional_answer");
        String right_answer = request.getParameter("right_answer");

        int kind_int = 0;
        if (kindQuestion.equals("Open Question")) {
            kind_int = 1;
        }
        if (kindQuestion.equals("True/False Question")) {
            kind_int = 2;
        }
        if (kindQuestion.equals("MultiChoice Question")) {
            kind_int = 3;
        }

        write_to_file_this_question(kind_int, category, value_int, question, optional_answer, right_answer);

//          write_to_file_this_question();
        String docType
                = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 "
                + "Transitional//EN\">\n";
        out.println(docType);
        out.println("<HTML>\n");
        out.println("<HEAD><TITLE> SuccessAdded</TITLE></HEAD>\n");
        out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"iFramebody.css\">\n");
        out.print("<BODY >\n");

        out.print("<div style=\"border: 0; width: 130px; height: 20px;position:absolute;\n"
                + "top: 0; right:0; text-align: right; margin-right: 4px;\">\n"
                + username
                + "</div>\n");

        out.print("<h2>" + question + "</h2>");
        out.print("<h2>" + global_path + "</h2>");
        out.print("</BODY>\n");
        out.print("</HTML>\n");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void write_to_file_this_question(int kindQuestion, String category, int value, String question, String op_Answer, String rght_Answer) {//will add question to the file!!!!

        TDQ = new TriviaDataQuestion(category, kindQuestion, value, question, op_Answer, rght_Answer);

        arrListAllQuestions = null;////////////need check in future
        arrListAllQuestions = new ArrayList();
        readAllDataFromFileToArrayList();//now ArrayList has questions from file
        addQuestionsToArrayList(TDQ);

        addDataToFile(arrListAllQuestions);//write ArrayList with added question back to file
    }

    public void readAllDataFromFileToArrayList() {

//    String sRootPath = new File("").getAbsolutePath();
//    global_path=sRootPath+"/questions.dat";
        String path_str = getServletContext().getRealPath("/");
//File file = new File(path+"questions.dat");
        global_path = path_str + "\\" + "questions.dat";
        path_str = path_str + "\\" + "questions.dat";
        write_data_path = path_str;//I save this String path for future use in method addDataToFile()
        ArrayList<TriviaDataQuestion> arrlist = new ArrayList();
        int count = 0;
        Path path = Paths.get(path_str);
        if (Files.notExists(path)) {
            global_path = "File not exist,can't read)";
            return;
        }
        //if file EXISTS
        ArrayList<TriviaDataQuestion> arr = new ArrayList();
        boolean check = true;
        FileInputStream fileIn = null;
        ObjectInputStream in = null;
        try {
            fileIn = new FileInputStream(path_str);

        } catch (IOException ios) {
            //  ios.printStackTrace();
            global_path = "IOException_FileInputStream";
        }

        try {
            in = new ObjectInputStream(fileIn);

        } catch (IOException io) {
            // io.printStackTrace();
            global_path = "IOException_ObjectInputStream";
        }

        try {

            //count++;
            arr = (ArrayList<TriviaDataQuestion>) in.readObject();
            //arrlist.add();////////////////////////////////////
            arrListAllQuestions = arr;//global ArrayList!!!!!!!!!!!!!!!!!!
        } catch (EOFException eof) {
            check = false;
            global_path = "EOFException_arr"; //not real exception works good!!!!!
        } catch (IOException i) {
            // i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {

//         System.out.println("Question class not found");
            global_path = "Question class not found";
            //c.printStackTrace();
            return;
        }

        try {
            in.close();
        } catch (IOException e) {
            // e.printStackTrace();
            global_path = "IOException_close_in";
        }
        try {
            fileIn.close();
        } catch (IOException ee) {
            //  ee.printStackTrace();
            global_path = "IOException_filein_close";
        }

    }

    public void addQuestionsToArrayList(TriviaDataQuestion s) {

        arrListAllQuestions.add(s);

    }

    public void addDataToFile(ArrayList list) {
        try {
            Path path = Paths.get(write_data_path);

            if (Files.notExists(path)) {
              // System.out.println("File not exist");

                File f = new File(write_data_path);
                f.createNewFile();
            }

            path = Paths.get(write_data_path);

        //if (Files.exists(path)){
            //System.out.println("File now exist");
            //}
        }//end try create new File
        catch (IOException e) {
            //e.printStackTrace();
            global_path = "IOException in method addDataToFile";
        }
    //now we want Write to File one question

        try {
            FileOutputStream fileOut
                    = new FileOutputStream(write_data_path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);

            out.writeObject(list);

            out.close();
            fileOut.close();
            // System.out.println("Serialized data is saved in questions.dat");
        } catch (IOException i) {
            // i.printStackTrace();
            global_path = "IOException in method addDataToFile FileOutputStream after close";
        }

    }//end function addDataToFile
}
