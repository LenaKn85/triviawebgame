package Logic;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
@WebServlet(name = "AddQuestionPage", urlPatterns = {"/AddQuestionPage"})
public class AddQuestionPage extends HttpServlet {

    String[] categoryArrstr = {"Art & Artists", "Architecture", "Ancient History", "The Bible", "Cities", "Cinema", "Animal Kingdom", "Astronomy and Space", "Colleges and Universities", "Modern History"};

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        String username;
        username = (String) session.getAttribute("username");

        String docType
                = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 "
                + "Transitional//EN\">\n";
        out.println(docType);
        out.println("<HTML>\n");
        out.println("<HEAD><TITLE> AdminPage</TITLE></HEAD>\n");
        out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"iFramebody.css\">\n");
        out.print("<BODY >\n");
        out.print("<div style=\"border: 0; width: 130px; height: 20px;position:absolute;\n"
                + "top: 0; right:0; text-align: right; margin-right: 4px;\">\n"
                + username
                + "</div>\n");

        out.print("<FORM Action=\"SuccessAddedQuestion\">");//in this servlet we will add question to file ,remove or show all list of questions to admin!!!!
        //User choose kind of question
        out.print("<div align=\"center\"><br>");
        out.print("<p style=\"color:white\"> <span style=\";font-family:Monotype Corsiva;font-size:22px;color:white;\">Kind of question:</span><br>");
        out.print("<input type=\"radio\" name=\"kind\" value=\"Open Question\"> Open Question<br>");
        out.print("<input type=\"radio\" name=\"kind\" value=\"True/False Question\"> True/False Question<br>");
        out.print("<input type=\"radio\" name=\"kind\" value=\"MultiChoice Question\"> MultiChoice Question<br>");
        out.print("</div>");

        //radio buttons of categories
        out.print("<div align=\"center\"><br>");
        out.print("<p style=\"color:white\"> <span style=\";font-family:Monotype Corsiva;font-size:22px;color:white;\">Choose category:</span><br>");
        for (int i = 0; i < categoryArrstr.length; i++) {
            out.print("<INPUT TYPE=\"radio\" name=\"category_name\" value=\"" + categoryArrstr[i] + "\">" + categoryArrstr[i] + "<BR>");
        }
        out.print("</div>");

        //write question
        out.print("<p style=\"color:white\"> <span style=\";font-family:Monotype Corsiva;font-size:22px;color:white;\">Write your question:</span><input type=\"text\" id=\"pass\" class=\"old\" size=\"100\" name=\"question\"><br> ");
        //OPTIONAL ANSWER
        out.print("<p style=\"color:white\"> <span style=\";font-family:Monotype Corsiva;font-size:22px;color:white;\">Optional answer:</span><input type=\"text\" id=\"pass\" class=\"old\" size=\"100\" name=\"optional_answer\"><br> ");

        //Right Answer
        out.print("<p style=\"color:white\"> <span style=\";font-family:Monotype Corsiva;font-size:22px;color:white;\">Right answer:</span><input type=\"text\" id=\"pass\" class=\"old\" size=\"100\" name=\"right_answer\"><br> ");

        //PLEASE CHOOSE VALUE OF QUESTION
        out.print("<div align=\"center\"><br>");
        out.print("<p style=\"color:white\"> <span style=\";font-family:Monotype Corsiva;font-size:22px;color:white;\">Value of question:</span><br>");
        out.print("<input type=\"radio\" name=\"values\" value=\"1\"> 1<br>");
        out.print("<input type=\"radio\" name=\"values\" value=\"2\"> 2<br>");
        out.print("<input type=\"radio\" name=\"values\" value=\"3\">3<br>");
        out.print("<input type=\"radio\" name=\"values\" value=\"4\">4<br>");
        out.print("<input type=\"radio\" name=\"values\" value=\"5\"> 5<br>");
        out.print("<input type=\"radio\" name=\"values\" value=\"6\"> 6<br>");
        out.print("<input type=\"radio\" name=\"values\" value=\"7\">7<br>");
        out.print("<input type=\"radio\" name=\"values\" value=\"8\"> 8<br>");
        out.print("<input type=\"radio\" name=\"values\" value=\"9\"> 9<br>");
        out.print("<input type=\"radio\" name=\"values\" value=\"10\"> 10<br>");
        out.print("</div>");

        out.print("<p>\n");
        out.print("<CENTER>");
        out.print("<INPUT TYPE=\"SUBMIT\" name=\"send\" VALUE=\"Add Question\" >");
        out.print("</p>\n");
        out.print("</CENTER>");

        out.print("</FORM>");

        out.print("</BODY>\n");
        out.print("</HTML>\n");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
