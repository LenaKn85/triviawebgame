package Logic;

import java.io.Serializable;

public abstract class Question implements Serializable {

    abstract String printQuestion(String question);

}
