package Logic;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
@WebServlet(name = "WriteOrDelete_Show_Question", urlPatterns = {"/WriteOrDelete_Show_Question"})
public class WriteOrDelete_Show_Question extends HttpServlet {

    ArrayList<TriviaDataQuestion> arrListAllQuestions = new ArrayList();//global ArrayList

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        String username;
        username = (String) session.getAttribute("username");

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String docType
                = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 "
                + "Transitional//EN\">\n";
        out.println(docType);

//vitaschit' vse values,vzyt' ves' object Serializable i dobav;t' k nemy
        String buttonClicked = request.getParameter("my_button");

        if (buttonClicked.equals("REMOVE QUESTION")) {//ADMIN CHOOSED REMOVE QUESTION
            //we need show all questions with checkboxes
            out.println("<HTML>\n");
            out.println("<HEAD><TITLE> AdminPage</TITLE></HEAD>\n");
            out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"iFramebody.css\">\n");
            out.print("<BODY >\n");

            out.print("<div style=\"border: 0; width: 130px; height: 20px;position:absolute;\n"
                    + "top: 0; right:0; text-align: right; margin-right: 4px;\">\n"
                    + username
                    + "</div>\n");

            out.print("<h2>clicked remove question button!</h2>");

            out.print("</BODY>\n");
            out.print("</HTML>\n");
        }

        if (buttonClicked.equals("ADD QUESTION")) {// admin choosed ADD QUESTION
            //we will read oll Serializible from file
            //add to "him" our questin and write again to file!!!!!
            out.println("<HTML>\n");
            out.println("<HEAD><TITLE> ADD QUESTION </TITLE></HEAD>\n");
            out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"iFramebody.css\">\n");
            out.print("<BODY >\n");
            //out.print("<h2>clicked add question button</h2>");

            out.print("</BODY>\n");
            out.print("</HTML>\n");

        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void readAllDataFromFileToArrayList() {
        //check if file exist!!!
        ArrayList<TriviaDataQuestion> arrlist = new ArrayList();
        int count = 0;
        Path path = Paths.get("questions.dat");
        if (Files.notExists(path)) {
            System.out.println("File not exist,can't read");
            return;
        }

        ArrayList arr = null;
        boolean check = true;
        FileInputStream fileIn = null;
        ObjectInputStream in = null;
        try {
            fileIn = new FileInputStream("questions.dat");

        } catch (IOException ios) {
            ios.printStackTrace();
        }

        try {
            in = new ObjectInputStream(fileIn);

        } catch (IOException io) {
            io.printStackTrace();
        }

        while (check) {
            try {

                //count++;
                arr = (ArrayList<TriviaDataQuestion>) in.readObject();
                //arrlist.add();////////////////////////////////////
                arrListAllQuestions = arr;//global ArrayList!!!!!!!!!!!!!!!!!!
            } catch (EOFException eof) {
                check = false;
            } catch (IOException i) {
                i.printStackTrace();
                return;
            } catch (ClassNotFoundException c) {

                System.out.println("Question class not found");
                c.printStackTrace();
                return;
            }
        }//end while

        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fileIn.close();
        } catch (IOException ee) {
            ee.printStackTrace();
        }

    }

}
