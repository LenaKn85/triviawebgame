package Logic;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
@WebServlet(name = "AdminPage", urlPatterns = {"/AdminPage"})
public class AdminPage extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        String uName = request.getParameter("username");
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        if (username == null) {
            //username = new String("guest");
            session.setAttribute("username", uName);
        }
        
        
        String formURL
                = "AddQuestionPage";
        // Pass URLs that reference own site through encodeURL.
        formURL = response.encodeURL(formURL);

        String docType
                = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 "
                + "Transitional//EN\">\n";
        out.println(docType);
        out.println("<HTML>\n");
        out.println("<HEAD><TITLE> AdminPage</TITLE></HEAD>\n");
        out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"iFramebody.css\">\n");
        out.print("<BODY >\n");
        out.print("<div style=\"border: 0; width: 130px; height: 20px;position:absolute;\n"
                + "top: 0; right:0; text-align: right; margin-right: 4px;\">\n"
                + uName
                + "</div>\n");
        out.print("<h2>Hello admin!!Nice to see you!!!</h2>");
        //Admin click on Add QuestionPage 
        out.print("<FORM Action=\"" + formURL + "\">");
        out.print("<p>\n");
        out.print("<CENTER>");
        out.print("<INPUT TYPE=\"SUBMIT\" name=\"my_button\" VALUE=\"Add Question\" >");
        out.print("</CENTER>");
        out.print("</p>\n");
        out.print("</FORM>");

        //Admin Clicks on RemoveQuestionPage
        formURL = response.encodeURL("RemoveQuestionPage");
        out.print("<FORM Action=\"" + formURL + "\" method=\"GET\">");
        out.print("<p>\n");
        out.print("<CENTER>");
        out.print("<INPUT TYPE=\"SUBMIT\"  name=\"my_button\" VALUE=\"Remove Question\" >");
        out.print("</CENTER>");
        out.print("</p>\n");
        out.print("</FORM>");

        out.print("</BODY>\n");
        out.print("</HTML>\n");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
