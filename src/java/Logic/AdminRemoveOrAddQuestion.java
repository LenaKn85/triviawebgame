
package Logic;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */


@WebServlet(name = "AdminRemoveOrAddQuestion", urlPatterns = {"/AdminRemoveOrAddQuestion"})
public class AdminRemoveOrAddQuestion extends HttpServlet {
String[] categoryArrstr={ "Art & Artists", "Architecture", "Ancient History","The Bible" ,"Cities","Cinema","Animal Kingdom", "Astronomy and Space","Colleges and Universities","Modern History"};
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
         response.setContentType("text/html");
         PrintWriter out = response.getWriter();
         
         HttpSession session = request.getSession();

        String username;
        username = (String) session.getAttribute("username");
        
        String docType =
      "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " +
      "Transitional//EN\">\n";
    out.println(docType);
    out.println("<HTML>\n");
    out.println("<HEAD><TITLE> AdminPage</TITLE></HEAD>\n");
    out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"iFramebody.css\">\n");
    out.print("<BODY >\n");
    
    out.print("<link rel=\"stylesheet\" href=\"categories.css\" type=\"text/css\">\n"
                    + "<div style=\"border: 0; width: 130px; height: 20px;position:absolute;\n"
                    + "top: 0; right:0; text-align: right; margin-right: 4px;\">\n"
                    + username
                    + "</div>\n");
    
     out.print("<h2></h2>");
    if(request.getParameter("add")!=null){
       // out.print("<p style=\"color:white\"> <span style=\";font-family:Monotype Corsiva;font-size:22px;color:white;\">Write your question:</span><input type=\"text\" id=\"pass\" class=\"old\" size=\"100\" name=\"add_question\"><br> ");
       
       out.print("<FORM Action=\"WriteOrDelete_Show_Questions\">");//in this servlet we will add question to file ,remove or show all list of questions to admin!!!!
        //User choose kind of question
        out.print("<div align=\"center\"><br>") ;
        out.print("<p style=\"color:white\"> <span style=\";font-family:Monotype Corsiva;font-size:22px;color:white;\">Kind of question:</span><br>");
        out.print("<input type=\"radio\" name=\"kind\" value=\"Open Question\"> Open Question<br>");
        out.print("<input type=\"radio\" name=\"kind\" value=\"True/False Question\"> True/False Question<br>");
       out.print("<input type=\"radio\" name=\"kind\" value=\"MultiChoice Question\"> MultiChoice Question<br>");
        out.print("</div>") ;
         
       
      
       //radio buttons of categories
        out.print("<div align=\"center\"><br>") ;
        out.print("<p style=\"color:white\"> <span style=\";font-family:Monotype Corsiva;font-size:22px;color:white;\">Choose category:</span><br>");
        for(int i=0;i<categoryArrstr.length;i++){
         out.print("<INPUT TYPE=\"radio\" name=\"category_name\" value=\""+categoryArrstr[i]+"\">"+categoryArrstr[i]+"<BR>");
        }
       out.print("</div>") ;
       
      //write question
       out.print("<p style=\"color:white\"> <span style=\";font-family:Monotype Corsiva;font-size:22px;color:white;\">Write your question:</span><input type=\"text\" id=\"pass\" class=\"old\" size=\"100\" name=\"add_question\"><br> ");
      //OPTIONAL ANSWER
       out.print("<p style=\"color:white\"> <span style=\";font-family:Monotype Corsiva;font-size:22px;color:white;\">Optional answer:</span><input type=\"text\" id=\"pass\" class=\"old\" size=\"100\" name=\"add_question\"><br> ");
       
       //PLEASE CHOOSE VALUE OF QUESTION
        out.print("<div align=\"center\"><br>") ;
        out.print("<p style=\"color:white\"> <span style=\";font-family:Monotype Corsiva;font-size:22px;color:white;\">Value of question:</span><br>");
        out.print("<input type=\"radio\" name=\"kind\" value=\"1\"> 1<br>");
        out.print("<input type=\"radio\" name=\"kind\" value=\"2\"> 2<br>");
          out.print("<input type=\"radio\" name=\"kind\" value=\"3\">3<br>");
           out.print("<input type=\"radio\" name=\"kind\" value=\"1\">4<br>");
        out.print("<input type=\"radio\" name=\"kind\" value=\"2\"> 5<br>");
          out.print("<input type=\"radio\" name=\"kind\" value=\"3\"> 6<br>");
           out.print("<input type=\"radio\" name=\"kind\" value=\"1\">7<br>");
        out.print("<input type=\"radio\" name=\"kind\" value=\"2\"> 8<br>");
          out.print("<input type=\"radio\" name=\"kind\" value=\"3\"> 9<br>");
           out.print("<input type=\"radio\" name=\"kind\" value=\"1\"> 10<br>");
        out.print("</div>") ;
        
       out.print("<p>\n");
       out.print("<CENTER>");
       out.print("<INPUT TYPE=\"SUBMIT\" name=\"send\" VALUE=\"Add Question\" >");
       out.print("</p>\n");
       out.print("</CENTER>");
out.print("</FORM>");

    if(request.getParameter("remove")!=null){//when iser choose remove we need show all questions with checkboxes!!!!!!
        
        out.print("<h2>remove</h2>");
    }
    
    out.print("</BODY>\n");
    out.print("</HTML>\n");
    
       }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
