 
package Logic;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */


@WebServlet(name = "RemoveQuestionPage", urlPatterns = {"/RemoveQuestionPage"})
public class RemoveQuestionPage extends HttpServlet {

    ArrayList<TriviaDataQuestion> arrListAllQuestions =new ArrayList();//global ArrayList
    String global_path;
    String write_data_path;
    
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    
  }
   @Override
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int key=0;
       response.setContentType("text/html");
          PrintWriter out = response.getWriter();
            String docType =
      "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " +
      "Transitional//EN\">\n";
    out.println(docType);
    out.println("<HTML>\n");
    out.println("<HEAD><TITLE> SuccessAdded</TITLE></HEAD>\n");
    out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"iFramebody.css\">\n");
  
    out.print("<BODY >\n");
    
    HttpSession session = request.getSession();

        String username;
        username = (String) session.getAttribute("username");
    
    out.print("<div style=\"border: 0; width: 130px; height: 20px;position:absolute;\n"
                + "top: 0; right:0; text-align: right; margin-right: 4px;\">\n"
                + username
                + "</div>\n");
    
     readAllDataFromFileToArrayList();
        
                               if(arrListAllQuestions.isEmpty()){
                                      //write to file that file is empty
                                      //print message
                                     out.print("<h1>File is empty</h1>");
                                    key=1;   
                               }
                         //show all questions to Admin
  // showAllChoosedQuestions(arrListAllQuestions);//show to Admin all question from file
  
    //like function show all questions
   if(key==0){//if  file is empty
    int i=1; 
  out.print("<FORM Action=\"RemoveFromFile\" method=\"GET\">");//I will Send it to other Servlet for DELETE
    out.print("<h2>Please choose files to DELETE:</h2>");
    out.print("<table border=\"1\" align=\"center\">");
    for(int y=0;y<arrListAllQuestions.size();y++){
             TriviaDataQuestion quest=(TriviaDataQuestion)arrListAllQuestions.get(y);
             //System.out.println(Integer.toString( i )+" "+quest.question);
             out.print("<tr bgcolor=\"aqua\">");
             out.print("<td><input type=\"checkbox\" name=\"questions\" value=\"" +y+ "\"  /></td>");
            out.print("<td><h4>"+quest.question+"</h4></td>");
             out.print("</tr>");
             i++;
         }
     
     out.print("</table>");
     
       out.print("<p>\n");
       out.print("<CENTER>");
       out.print("<INPUT TYPE=\"SUBMIT\" name=\"remove\" VALUE=\"REMOVE\" >");
       out.print("</p>\n");
       out.print("</CENTER>");
    
       out.print("</FORM>");
     out.print("</BODY>\n");
     out.print("</HTML>\n");
      }
       
    }
   
   @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
public void readAllDataFromFileToArrayList(){
   
//    String sRootPath = new File("").getAbsolutePath();
//    global_path=sRootPath+"/questions.dat";
  
String path_str = getServletContext().getRealPath("/");
//File file = new File(path+"questions.dat");
global_path=path_str+"\\"+"questions.dat";
path_str=path_str+"\\"+"questions.dat";
  write_data_path=path_str;//I save this String path for future use in method addDataToFile()
ArrayList <TriviaDataQuestion> arrlist = new ArrayList( );
        int count=0;
       Path path = Paths.get(path_str);
        if (Files.notExists(path)){
          global_path="File not exist,can't read)";
        return;
        }      
               //if file EXISTS
         ArrayList<TriviaDataQuestion> arr=new ArrayList();
         boolean check=true;
         FileInputStream fileIn=null;
         ObjectInputStream in=null;
         try{
         fileIn = new FileInputStream(path_str);
         
     
         }
        catch(IOException ios){
           //  ios.printStackTrace();
             global_path="IOException_FileInputStream";
         }
         
         
         try{
         in = new ObjectInputStream(fileIn);
         
     
         }
         catch(IOException io){
            // io.printStackTrace();
              global_path="IOException_ObjectInputStream";
         }
         
        
        try
            {
         
         //count++;
         arr = (ArrayList<TriviaDataQuestion>)in.readObject();
         //arrlist.add();////////////////////////////////////
            arrListAllQuestions=arr;//global ArrayList!!!!!!!!!!!!!!!!!!
            }
         
        catch(EOFException eof){
             check=false;
              //global_path="EOFException_arr"; not real exception works good!!!!!
         }
       catch(IOException i)
       {
        // i.printStackTrace();
         return;
       }
        catch(ClassNotFoundException c){
      
//         System.out.println("Question class not found");
         global_path="Question class not found";
         //c.printStackTrace();
         return;
          }
         
         
         try{
         in.close();
         }
         catch(IOException e){
            // e.printStackTrace();
               global_path="IOException_close_in";
         }
        try{
         fileIn.close();
         }
         catch(IOException ee){
           //  ee.printStackTrace();
             global_path="IOException_filein_close";
         }
   
}


//public void showAllChoosedQuestions(ArrayList arrlist){
//    int i=0; 
//    for(int y=0;y<arrlist.size();y++){
//             TriviaDataQuestion quest=(TriviaDataQuestion)arrlist.get(y);
//             //System.out.println(Integer.toString( i )+" "+quest.question);
//            out.print("<h1>"+Integer.toString( i )+" "+quest.question+"</h1>");
//             
//             i++;
//         }
//    
//}


}
