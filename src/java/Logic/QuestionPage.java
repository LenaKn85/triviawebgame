package Logic;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
@WebServlet(name = "QuestionPage", urlPatterns = {"/QuestionPage"})
public class QuestionPage extends HttpServlet {

    int count_score;
    String[] categoryArrstr = {"Art & Artists", "Architecture", "Ancient History", "The Bible", "Cities", "Cinema", "Animal Kingdom", "Astronomy and Space", "Colleges and Universities", "Modern History"};

    Map<Integer, String> testMap = new HashMap<Integer, String>();

    ArrayList<TriviaDataQuestion> arrListAllQuestions;  // =new ArrayList();//global ArrayList
    ArrayList<TriviaDataQuestion> temp;    // =new ArrayList();//sorted questions that USER cchoosed by Category and Value of points
    static ArrayList<TriviaDataQuestion> randList;
    TriviaDataQuestion TDQ = null;

    String global_path;
    String write_data_path;

    public void init() throws ServletException {

        arrListAllQuestions = new ArrayList();//global ArrayList
        temp = new ArrayList();//sorted questions that USER cchoosed by Category and Value of points
//we read all question sfrom file        
        readAllDataFromFileToArrayList();

//need Sort them (now only by Category,in future will sort by Low,Medium,Hard)
        temp = sortOnlyCategoryUserChoosed(arrListAllQuestions, categoryArrstr);
//Radomize all TriviaDataQuestion that serializable
        randList = randomQuestionsInArrayList(temp);

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        HttpSession session = request.getSession();

        String username;
        username = (String) session.getAttribute("username");

        String docType
                = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 "
                + "Transitional//EN\">\n";
        out.println(docType
                + "<HTML>\n"
                + "<HEAD><TITLE>" + "Questions" + "</TITLE>\n"
                + "<link rel=\"stylesheet\" type=\"text/css\" href=\"iFramebody.css\">\n"
                + "</HEAD>\n"
                + "<BODY>\n"
                
                + "<div style=\"border: 0; width: 130px; height: 20px;position:absolute;\n"
                + "top: 0; right:0; text-align: right; margin-right: 4px;\">\n"
                + username
                + "</div>\n"
                
                + "<H1>" + "Chosen questions from next categories:" + "</H1>");
        String[] results;
        int count = 0;
        for (int i = 0; i < categoryArrstr.length; i++) {
            results = request.getParameterValues(categoryArrstr[i]);//if was checkbox category checked

            if (results != null) {
                for (int j = 0; j < results.length; j++) {
                    String value = request.getParameter(categoryArrstr[i] + "_diff");
                    out.print("<table class=\"text\"align=\"center\"bgcolor=\"#00FFFF\">");
                    out.print("<tr>");
                    out.print("<td><H3>" + categoryArrstr[i] + "</H3></td>");//Name of category and Level
                    out.print("<td><H4>" + value + "</H4></td>");
                    out.print("</tr>");
                    out.print("</table> ");
                    //tyt budem vibirati voprosi Random!!!!!
                    count++;

                }
            }

        }
        out.print("<p></p>");

//readAllDataFromFileToArrayList();
//
//temp=sortOnlyCategoryUserChoosed(arrListAllQuestions,categoryArrstr);
//
//
//ArrayList<TriviaDataQuestion> randList;
// randList=randomQuestionsInArrayList(temp);
// ArrayList<TriviaDataQuestion> randList;
// randList=randomQuestionsInArrayList(temp);
        String[] rightAnswers = new String[randList.size()];//need write all answers to here
        int[] scores = new int[randList.size()];//write all scores to here

        out.print("<FORM Action=\"CheckAnswersCountScores\">");//we need send in hidden value at Right Answers and scores PER ONE QUESTION
        for (int i = 0; i < randList.size(); i++) {
            out.print("<table class=\"text\"align=\"center\" bgcolor=\"#00FF00\" width=\"600\" height=\"100\">");//Table 
            out.print("<p></p>");
            TriviaDataQuestion tmpData;
            Question qs;
            tmpData = (TriviaDataQuestion) randList.get(i);
            qs = tmpData.getType();
            String question = qs.printQuestion(tmpData.question);

            //save answer and scores and save it in hidden field
            rightAnswers[i] = tmpData.answer;//save answer for specific question
            scores[i] = tmpData.value;
        //out.print("<h2>"+rightAnswers[i]+"</h2>");
            //out.print("<h2>"+scores[i]+"</h2>");

            if (qs instanceof MultiChoiceQuestion) {
                out.print("<tr>");
                out.print("<td><h3>" + question + "<h3></td>");//row with question
                String optional = qs.printQuestion(tmpData.getoptionalAnswer());
                out.print("<td><h4>Choose one or more answers:<h4>");
                String[] words = optional.split(",");//divide string to words
                for (String word : words) {
                    // System.out.println(tokenizer.nextToken());
                    out.print("<input type=\"checkbox\"  name=\"" + "checkboxes" + i + "\" value=\"" + word + "\">" + word + "<br>\n");

                }
                out.print("</td>");//end row
                out.print("</tr>");
//  out.print("<h2>"+optional+"<h2>");
            }
            if (qs instanceof OpenQuestion) {
                //budet input type
                out.print("<tr>\n");
                out.print("<td><h3>" + question + "<h3></td>");//row with question
                out.print("<td><h4>Write your answer:<h4></td>");
                out.print("<td><input type=\"text\" size=\"8\" name=\"" + "checkboxes" + i + "\"></td>");
                out.print("</tr>\n");
            }
            if (qs instanceof TrueFalseQuestion) {
                //chechboxes
                out.print("<tr>\n");
                out.print("<td><h3>" + question + "<h3></td>\n");//row with question

                out.print("<td>\n");
                out.print("<input type=\"checkbox\"  name=\"" + "checkboxes" + i + "\" value=\"Yes\">Yes<br>\n");
                out.print("</td>\n");

                out.print("<td>\n");
                out.print("<input type=\"checkbox\"  name=\"" + "checkboxes" + i + "\" value=\"No\">No<br>\n");
                out.print("</td>\n");

                out.print("<tr>\n");
            }

            out.print("</table >\n");//Table ends

// String inputUser=console.nextLine();
            //tmpData.getScore(inputUser);
            // count_score+=tmpData.getScore(inputUser);
        }

        out.print("<input type=\"hidden\" name=\"answers\" value=\"" + rightAnswers + "\">");
        out.print("<input type=\"hidden\" name=\"scores\" value=\"" + scores + "\">");
        out.print("<p></p>\n");
        out.print("<input type=\"submit\" value=\"SEND\" style=\"height:50px; width:80px\" background=\"#00FFFF\" >\n");
        out.print("</FORM>");

        if (count == 0) { //just detection if user not choosed categories do pageworward

            //user not choosed categories
            String nextPage = "/CategoryPage";
            RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
            dispatcher.forward(request, response);
        }

        out.println("</BODY></HTML>");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void readAllDataFromFileToArrayList() {
        String path_str = getServletContext().getRealPath("/");
//File file = new File(path+"questions.dat");
        global_path = path_str + "\\" + "questions.dat";
        path_str = path_str + "\\" + "questions.dat";
        write_data_path = path_str;//I save this String path for future use in method addDataToFile()
        ArrayList<TriviaDataQuestion> arrlist = new ArrayList();
        int count = 0;
        Path path = Paths.get(path_str);
        if (Files.notExists(path)) {
            global_path = "File not exist,can't read)";
            return;
        }
        //if file EXISTS
        ArrayList<TriviaDataQuestion> arr = new ArrayList();
        boolean check = true;
        FileInputStream fileIn = null;
        ObjectInputStream in = null;
        try {
            fileIn = new FileInputStream(path_str);

        } catch (IOException ios) {
            //  ios.printStackTrace();
            global_path = "IOException_FileInputStream";
        }

        try {
            in = new ObjectInputStream(fileIn);

        } catch (IOException io) {
            // io.printStackTrace();
            global_path = "IOException_ObjectInputStream";
        }

        try {

            //count++;
            arr = (ArrayList<TriviaDataQuestion>) in.readObject();
            //arrlist.add();////////////////////////////////////
            arrListAllQuestions = arr;//global ArrayList!!!!!!!!!!!!!!!!!!
        } catch (EOFException eof) {
            check = false;
            global_path = "EOFException_arr"; //not real exception works good!!!!!
        } catch (IOException i) {
            // i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {

//         System.out.println("Question class not found");
            global_path = "Question class not found";
            //c.printStackTrace();
            return;
        }

        try {
            in.close();
        } catch (IOException e) {
            // e.printStackTrace();
            global_path = "IOException_close_in";
        }
        try {
            fileIn.close();
        } catch (IOException ee) {
            //  ee.printStackTrace();
            global_path = "IOException_filein_close";
        }

    }

    public ArrayList sortOnlyCategoryUserChoosed(ArrayList<TriviaDataQuestion> arrListQuest, String[] categoryArrayStr) {//may return ArrayList sorted CATEGORIES!!
        ArrayList<TriviaDataQuestion> sortedListByCategory = new ArrayList();

//     for (int i = 0; i < arrl.size(); i++) {
//         TriviaDataQuestion allq=(TriviaDataQuestion)arrl.get(i);
//         for(int y=0;y<categoryArrayStr.length;y++){
//            // Question userCarQ=(Question)categoryList.get(y);
//             String category = categoryArrayStr[y];
//             if(allq.category.equals(category)){//poka ne categoriy!!!
//                 sortedListByCategory.add(allq);
//                 
//             }
//         }
        //  }
        int count = 0;
        for (int i = 0; i < arrListQuest.size(); i++) {
            for (int y = 0; y < categoryArrayStr.length; y++) {
                TriviaDataQuestion allq = (TriviaDataQuestion) arrListQuest.get(i);
                if (allq.category.equals(categoryArrayStr[y])) {
                    sortedListByCategory.add(allq);
                }

            }
        }

        return sortedListByCategory;
    }

    public ArrayList randomQuestionsInArrayList(ArrayList lista) {

        Collections.shuffle(lista);
        return lista;
    }

//public void  startPlay(ArrayList randList){
//  
//   for(int i=0;i<randList.size();i++){
//     
//       TriviaDataQuestion tmpData;
//        Question qs;
//        tmpData=(TriviaDataQuestion)randList.get(i);
//        qs=tmpData.getType();
//        qs.printQuestion(tmpData.question);
//        if(qs instanceof MultiChoiceQuestion){
//            qs.printQuestion(tmpData.getoptionalAnswer());
//        }
//        String inputUser=console.nextLine();
//        //tmpData.getScore(inputUser);
//       count_score+=tmpData.getScore(inputUser);
//    }
//System.out.println("Congratulations your finaly score is:"+Integer.toString(count_score));
//System.out.println("program created by Dani Livshits");
//}
}//end of QuestionPage