import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
@WebServlet(urlPatterns = {"/WellComeServlet"})
public class WellComeServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        /* need ckeck id user want be REMEMBER */

        String uName = request.getParameter("username");
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        if (username == null) {
            //username = new String("guest");
            session.setAttribute("username", uName);
        }
        /*
         HttpSession session = request.getSession();
         String username = (String) session.getAttribute("username");
         if (username == null) {
         username = new String("guest");
         session.setAttribute("username", username);
         }*/

        String active = request.getParameter("checkBoxLogin");
        if (active != null) {
            // check box is selected

            boolean newbie = true;

            Cookie[] cookies = request.getCookies();
            if (cookies != null) {

                for (int i = 0; i < cookies.length; i++) {
                    Cookie c = cookies[i];
                    if (c.getName().equals("repeatVisitor") && c.getValue().equals("yes")) {
                        newbie = false;

                    }

                }
            }
            String title;
            int count = 0;

            if (newbie) {
                Cookie returnVisitorCookie = new Cookie("repeatVisitor", "yes");

                returnVisitorCookie.setMaxAge(60 * 60); // 1 year

                response.addCookie(returnVisitorCookie);

                title = "Welcome New User:";

            } else {
                title = "Welcome Back:";
                count = 1;
            }

            String docType
                    = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 "
                    + "Transitional//EN\">\n";
            out.println(docType);
            out.println("<HTML>\n");
            out.println("<HEAD><TITLE> Welcome Page</TITLE></HEAD>\n");
            out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"iFramebody.css\">\n");
            out.print("<BODY >\n");

            if (count == 0) {
                out.print("<h1><font color=\"white\">Wellcome New User:</font><h1>" + uName);
            }
            if (count == 1) {
                out.print("<h1><font color=\"white\">Wellcome Back:</font><h1>" + uName);
            }
            count = 0;
            out.print("<h2><font color=\"white\">Click Start on Menu selection and game will start!!!</font><h2>");

            out.print("</BODY>\n");
            out.print("</HTML>\n");

        } else {/*user didn't want be REMEBER */

            String docType
                    = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 "
                    + "Transitional//EN\">\n";
            out.println(docType);
            out.println("<HTML>\n");
            out.println("<HEAD><TITLE> Welcome Page</TITLE></HEAD>\n");
            out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"iFramebody.css\">\n");
            out.print("<BODY >\n");

            out.print("<h1><font color=\"white\">Wellcome New User:</font><h1>" + uName);

            out.print("<form action=\"CategoryPage\" method=\"POST\">\n"
                    + "<input type=\"submit\" value=\"Start the Game\">\n");
            out.print("<br>");
            out.print("</form>");
            out.print("</BODY>\n");
            out.print("</HTML>\n");

        }
    }
}
